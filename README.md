This project is inspired from [Render Feature Tree In HTML Page](https://www.codestack.net/solidworks-api/getting-started/scripts/java-script/html-feature-tree/)


Check out the Excel version [Generate-SW-Part-From-Excel](https://gitlab.com/testingusername/generate-sw-catia-part-from-excel)


Go to https://fatihmeh.github.io/generate-sw-part/ and work with it as a classic web page based on Github Pages


<hr>


Open [test-dim-inject](/test-dim-inject.html) file in Internet Explorer. Accept warning prompts. Fill required fields and click the button. Check preconditions.


Using a template part is more useful than creating a new one every time. You can use every kind of part you have as template when you change dimensions in code.


My template file is in 2018, to use this macro in another version just create a part with the same dimension names as in code.


SolidWorks part will be opened as read-only, so you can use <kbd>CTRL</kbd>+<kbd>S</kbd> to directly save your part to a new location.


**Preconditions:**
- SolidWorks opened and ready
- [Template part file](/template-html-inject.SLDPRT) downloaded to your disk
- Template file path needs to be entered in textbox

![0](/capture-html.PNG)


If you you don't see anything happening, please check your Internet explorer settings and appy this one below:

![0](/capture-ie-setting.PNG)